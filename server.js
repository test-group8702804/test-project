const express = require('express')
const cors = require('cors')
const { response } = require('express')

var app = express()

app.use(cors('*'))

app.use(express.json())

app.get('/',(request, response)=>{
    response.send('Running inside Container version 2')
})

app.listen(3000,'0.0.0.0',()=>{
    console.log('Server started at port 3000')
})